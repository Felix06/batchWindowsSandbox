@echo off

if [%1]==[] goto usage
goto :eof

:usage
@echo Usage: script_name.bat ^<application_name^>
@echo You didn't provide any arguments. Did you forget that you have those applications configured already ?
for /f "delims=" %%D in ('dir /a:d /b') do if not shouldt_display == %%D echo %%D
@echo if you don't know what i am talking about you should configure application prior to use this.
@echo Read the ReadMe and/or go to url to know more.

exit /B 1
