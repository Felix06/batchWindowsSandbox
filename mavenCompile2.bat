:: compile source from two mvn project (first one for war the second one to :: embed it in ear) and put the ear at some location.
::
@echo off
:: Here write your own path for eclipse workspace and relative path to 
:: eclipse workspace for your two projects and the target dir for ear.
:: If your two project dont reside in the same workspace just leave 
:: eclipseWorkspace empty and write full path in warSRC and earSrc.
set eclipseWorkspace=D:\Fef\eclipse
set warSrc=\Swagger\swagger2.xjersey2.x_swaggerui_integrated
set earSrc=\EarParent
set targetPathForEar=D:\batchWindowsFile
set a=%eclipseWorkspace%%warSrc%
set b=%eclipseWorkspace%%earSrc%
:: You can specify the ear name here if you have multiple ear in the same directory.
set c=*.ear
mvn -f %a% clean install && mvn -f %b% clean install && copy %b%\target\%c% %targetPathForEar% /Y && echo "Succes de la copie a l'emplacement : %targetPathForEar%" || echo "une erreur est survenue !" & pause

