:: compile source from two mvn project (first one for war the second one to :: embed it in ear) and put the ear at some location
::
@echo off
:: Here write your own path for eclipse workspace and relative path to 
:: eclipse workspace for your two projects and the target dir for ear
set eclipseWorkspace=D:\Fef\eclipse
set warSrc=\Swagger\swagger2.xjersey2.x_swaggerui_integrated
set earSrc=\EarParent
set targetPathForEar=D:\batchWindowsFile
set currentDir=%cd%
set a=%eclipseWorkspace%%warSrc%
set b=%eclipseWorkspace%%earSrc%
set c=*.ear
cd %a% && mvn clean install && cd %b% && mvn clean install && cd target && copy %c% %targetPathForEar% /Y && echo "Succes de la copie a l'emplacement : %targetPathForEar%" || echo "une erreur est survenue !" & cd %currentDir% & pause